using Hello
using Documenter

DocMeta.setdocmeta!(Hello, :DocTestSetup, :(using Hello); recursive = true)

makedocs(;
    modules = [Hello],
    authors = "Satoshi Terasaki <terasakisatoshi.math@gmail.com> and contributors",
    repo = "https://gitlab.com/terasakisatoshi/Hello.jl/blob/{commit}{path}#{line}",
    sitename = "Hello.jl",
    format = Documenter.HTML(;
        prettyurls = get(ENV, "CI", "false") == "true",
        canonical = "https://terasakisatoshi.gitlab.io/Hello.jl",
        edit_link = "main",
        assets = String[],
    ),
    pages = ["Home" => "index.md"],
)
