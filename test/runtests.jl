using Hello
using Test

@testset "Hello.jl" begin
    # Write your tests here.
    hello("world") == "Hello, world"
    Hello.mymath(3, 5) == 11
end
