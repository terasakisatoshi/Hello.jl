# Hello

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://terasakisatoshi.gitlab.io/Hello.jl/dev)
[![Build Status](https://github.com/terasakisatoshi/Hello.jl/actions/workflows/CI.yml/badge.svg?branch=main)](https://github.com/terasakisatoshi/Hello.jl/actions/workflows/CI.yml?query=branch%3Amain)
[![Build Status](https://gitlab.com/terasakisatoshi/Hello.jl/badges/main/pipeline.svg)](https://gitlab.com/terasakisatoshi/Hello.jl/pipelines)
[![Coverage](https://gitlab.com/terasakisatoshi/Hello.jl/badges/main/coverage.svg)](https://gitlab.com/terasakisatoshi/Hello.jl/commits/main)


This is just a hello world project for my own purpose. This Julia package is generated automatically with the following JuliaLang scrit:

```julia
using PkgTemplates

t = Template(;
    dir = pwd(),
    user = "terasakisatoshi",
    julia = v"1.9",
    host = "gitlab.com",
    plugins = [
        License(; name = "MIT"),
        Git(;
            ignore = [".DS_Store", "docs/build", ".ipynb_checkpoints", "*.ipynb"],
            manifest = false,
            ssh = true,
        ),
        GitLabCI(),
        Documenter{GitLabCI}(),
    ],
)

t("Hello.jl")
```


